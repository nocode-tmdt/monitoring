. ./env.sh

docker-compose -f docker-compose.yml -p $APP_NAME_DOCKER down

# sudo rm -rf "$APP_VOLUME_DATABASE_MOUNT_PATH"
sudo mkdir -p "$APP_VOLUME_FILES_MOUNT_PATH"
sudo mkdir -p "$APP_VOLUME_FILES_MOUNT_PATH/config"
sudo mkdir -p "$APP_VOLUME_DASHBOARD_MOUNT_PATH"
sudo touch "$APP_VOLUME_FILES_MOUNT_PATH/filebrowser.db"

docker-compose -f docker-compose.yml -p $APP_NAME_DOCKER up --build -d
